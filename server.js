const express = require('express');
const https = require('https');
const fs = require('fs');
// const config = require('getconfig');
// const sockets = require('signal-master/sockets');

const sslkey = fs.readFileSync('ssl-key.pem');
const sslcert = fs.readFileSync('ssl-cert.pem');

const options = {
      key: sslkey,
      cert: sslcert,
};

const app = express();

app.use(express.static('public'));

// HTTP Server
https.createServer(options, app).listen(3000);
console.log(`Listening on port 3000`);

// Signalling server
// const server = https.createServer(options, app).listen(8888);

// sockets(server, config);
